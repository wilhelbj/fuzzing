#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char *checkChar(char str[50]) {
  int i;
  char *newstr;
  newstr = (char *) calloc(strlen(str)*4+1,1);
   for (i = 0; i < strlen(str); i++) {
    if (str[i] == '&') {
      const char *ch = "&amp";
      strncat(newstr, ch, 4);
    } else if (str[i] == '<') {
      const char *ch = "&lt";
      strncat(newstr, ch, 3);
    } else if (str[i] == '>') {
      const char *ch = "&gt";
      strncat(newstr, ch, 3);
    } else {
      const char ch = str[i];
      strncat(newstr, &ch, 1);
    }
  }
  return newstr;
}

/*
int main() {
  char str[200];
  printf("Enter the string you want to convert: \n");
  fgets(str, 200, stdin);
  char *string = checkChar(str);
  printf("After replacement:\n%s", string);

  return 0;
}*/
